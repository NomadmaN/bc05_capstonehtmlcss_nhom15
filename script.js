// Owl Carousel (Testimonial section)
$(document).ready(function () {
  $("#owl-carousel-effect").owlCarousel({
    loop: true,
    margin: 20,
    nav: false,
    responsiveClass: true,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplaySpeed: 1000,
    autoplayHoverPause: false,
    responsive: {
      0: {
        items: 1,
        nav: false,
      },
      1000: {
        items: 1,
        nav: false,
        loop: false,
      },
    },
  });
});

// Back to Top button
$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $(".backtotop").fadeIn();
    } else {
      $(".backtotop").fadeOut();
    }
  });
  $(".backtotop").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 800);
    return false;
  });
});

// Switch Theme button
document.getElementById("switchTheme").onclick = function () {
  document.getElementById("farmsteadBody").classList.toggle("dark");
};

// Fixed Navbar
window.onscroll = function () {
  fixedNavbar();
};

var navbar = document.getElementById("main-header");
function fixedNavbar() {
  if (window.pageYOffset >= 100) {
    navbar.classList.add("nav-fixed");
  } else {
    navbar.classList.remove("nav-fixed");
  }
}
